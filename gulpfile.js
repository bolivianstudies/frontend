var
    gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    sass = require('gulp-sass'),
    useref = require('gulp-useref'),
    uglify = require('gulp-uglify'),
    cssnano = require('gulp-cssnano'),
    gulpIf = require('gulp-if'),
    imagemin = require('gulp-imagemin'),
    cache = require('gulp-cache'),
    del = require('del'),
    runSequence = require('run-sequence'),
    inject = require('gulp-inject'),
    bowerFiles = require('main-bower-files'),
    angularFilesort = require('gulp-angular-filesort'),
    templateCache = require('gulp-angular-templatecache'),
    replace = require('gulp-string-replace'),
    historyApiFallback = require('connect-history-api-fallback')
    ;

gulp.task('templates', function () {
    return gulp.src(['app/**/*.html', '!app/index.html'])
        .pipe(templateCache('app.templates.js', {
            module: 'app.templates', standalone: true, base: function (file) {
                return /[^/]*$/.exec(file.relative)[0];
            }
        }))
        .pipe(gulp.dest('app'));
});

gulp.task('inject', function () {
    return gulp.src('app/index.html')
        .pipe(inject(gulp.src(bowerFiles(), {read: false}), {name: 'bower', relative: true}))
        .pipe(inject(gulp.src(['app/**/*.css'], {read: false}), {relative: true}))
        .pipe(inject(gulp.src(['app/**/*.js'])
            .pipe(angularFilesort()), {relative: true}))
        .pipe(gulp.dest('app'));
});

gulp.task('inject:dist', function () {
    return gulp.src('app/index.html')
        .pipe(inject(gulp.src(bowerFiles(), {read: false}), {name: 'bower', relative: true}))
        .pipe(inject(gulp.src(['app/**/*.css'], {read: false}), {relative: true, addPrefix: '../app'}))
        .pipe(inject(gulp.src(['app/**/*.js'])
            .pipe(angularFilesort()), {relative: true, addPrefix: '../app'}))
        .pipe(gulp.dest('dist'));
});

gulp.task('useref', function () {
    return gulp.src('dist/*.html')
        .pipe(useref())
        .pipe(gulpIf('*.js', uglify()))
        .pipe(gulpIf('*.css', cssnano({safe: true})))
        .pipe(gulp.dest('dist'));
});

gulp.task('replace:dist', function () {
    return gulp.src('dist/js/app.min.js', {read: true})
        .pipe(replace('http://bolivianstudies:8000/', 'https://api.bolivianstudies.org/'))
        .pipe(gulp.dest('dist/js/'));
});

gulp.task('images', function () {
    return gulp.src('app/img/**/*.+(png|jpg|jpeg|gif|svg|ico)')
        .pipe(cache(imagemin({
            interlaced: true
        })))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('fonts', function () {
    return gulp.src(['app/fonts/**/*', 'lib/bootstrap/dist/fonts/**/*'])
        .pipe(gulp.dest('dist/fonts'))
});

gulp.task('mocks', function () {
    return gulp.src('app/mocks/**/*')
        .pipe(gulp.dest('dist/mocks'))
});

gulp.task('downloads', function () {
    return gulp.src('app/downloads/**/*')
        .pipe(gulp.dest('dist/downloads'))
});

gulp.task('locale', function () {
    return gulp.src('app/i18n/**/*')
        .pipe(gulp.dest('dist/i18n'))
});

gulp.task('apache', function () {
    return gulp.src('app/.htaccess')
        .pipe(gulp.dest('dist'))
});

gulp.task('clean', function () {
    return del.sync('dist').then(function (cb) {
        return cache.clearAll(cb);
    });
});

gulp.task('clean:dist', function () {
    return del.sync(['dist/**/*', '!dist/img', '!dist/img/**/*']);
});

gulp.task('sass', function () {
    return gulp.src("app/layout/main.scss")
        .pipe(sass())
        .pipe(gulp.dest("app/css"))
        .pipe(browserSync.stream());
});

gulp.task('browserSync', function () {
    browserSync.init({
        open: 'external',
        host: 'bolivianstudies',
        server: {
            baseDir: ['./app', './'],
            middleware: [historyApiFallback()]
        }
    });
});

gulp.task('watch', function () {
    gulp.watch("app/**/*.scss", ['sass']);
    gulp.watch("app/**/*.js", browserSync.reload);
    gulp.watch("app/**/*.json", browserSync.reload);
    gulp.watch("app/**/*.html", ['templates', browserSync.reload]);
});

gulp.task('build', function (callback) {
    runSequence('clean:dist', ['templates', 'sass'], 'inject:dist', ['useref', 'images', 'fonts', 'mocks', 'downloads', 'locale', 'apache'], 'replace:dist', callback)
});

gulp.task('default', function (callback) {
    runSequence(['templates', 'sass'], 'inject', ['browserSync', 'watch'],
        callback
    )
});

gulp.task('serve:dist', ['build'], function () {
    browserSync.init({
        open: 'external',
        host: 'bolivianstudies',
        server: {
            baseDir: './dist',
            middleware: [historyApiFallback()]
        }
    });
});

gulp.task('serve', function () {
    browserSync.init({
        open: 'external',
        host: 'bolivianstudies',
        server: {
            baseDir: './dist',
            middleware: [historyApiFallback()]
        }
    });
});