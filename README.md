![Bolivian Studies](app/img/assets/logo-dark.png)

# Bolivian Studies

A frontend application for the Asociación de Estudiso Bolivianos (AEB).

## Requirements

* [Node JS](https://nodejs.org/)
* [Bower](https://bower.io/)
* [Gulp](http://gulpjs.com/)

## Installation

### Node Modules (development tools)

Executing the `npm install` command on the terminal will take the [package.json](package.json) development dependencies and install them into the **node_modules** folder.

### Bower Dependencies (application libraries)

The `bower install` command will take the [bower.json](bower.json) application dependencies and install them into the **lib** folder.

## Development

Running the `gulp` command on the SSH Terminal will take the [app](app) source folder and run a local server watching file changes and reloading accordingly.

### Style Guide and Best Practices

It's mandatory that the file structure, modularity and code standards follow [John Papa's Style Guide](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md)  

## Production

The `gulp build` command will build the application from the [app](app) source folder and create a **dist** folder with optimized and minified assets
The `gulp serve:dist` command will build the into a **dist** folder and start a local server from that folder

