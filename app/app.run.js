(function () {
    'use strict';

    angular
        .module('app')
        .run(block);

    block.$inject = ['$rootScope', '$translate', '$transitions', '$state', 'authService'];

    function block($rootScope, $translate, $transitions, $state, authService) {

        $transitions.onStart({}, function ($transition$) {

            var toState = $transition$.$to();

            $rootScope.$broadcast('closeNav');

            if (toState.auth && !authService.isAuthenticated()) {
                $state.go('login');
                $rootScope.$broadcast('notify', {
                    message: $translate.instant('auth.messages.unauthorized'),
                    type: 'warning',
                    read: false
                });
            }

        });

    }
})();