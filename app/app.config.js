(function () {
    'use strict';

    angular
        .module('app')
        .config(configuration);

    configuration.$inject = ['$locationProvider', '$httpProvider'];

    function configuration($locationProvider, $httpProvider) {

        $httpProvider.defaults.headers.common['Accept'] = 'application/prs.aeb.v1+json';
        $httpProvider.interceptors.push('interceptorService');

        if (window.history && window.history.pushState) {
            $locationProvider.html5Mode(true);
        }

    }

})();