(function () {
    'use strict';

    var footer = {
        templateUrl: 'footer.component.html',
        controller: FooterController
    };

    angular
        .module('app.layout')
        .component('footer', footer);

    FooterController.$inject = ['$scope', 'settingsService', 'store'];

    function FooterController($scope, settingsService, store) {

        var $ctrl = this;
        var settings = settingsService.getSettings();
        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.footerEnabled = settings.footer.enabled;
        $ctrl.subscriptionEnabled = settings.subscription.enabled;

        $scope.$on('language', switchLanguage);

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
        }

    }

})();