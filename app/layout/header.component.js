(function () {
    'use strict';

    var header = {
        templateUrl: 'header.component.html',
        controller: HeaderController
    };

    angular
        .module('app.layout')
        .component('header', header);

    HeaderController.$inject = ['$rootScope', '$scope', '$state', '$transitions', 'settingsService', 'store'];

    function HeaderController($rootScope, $scope, $state, $transitions, settingsService, store) {

        var $ctrl = this;
        var settings = settingsService.getSettings();

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.isNavCollapsed = true;
        $ctrl.toggleNav = toggleNav;
        $ctrl.switchLang = switchLang;
        $ctrl.phone = settings.phone;
        $ctrl.email = settings.email;
        $scope.$on('closeNav', closeNav);

        $transitions.onStart({}, function () {
            $ctrl.user = store.get('user');
        });

        function toggleNav() {
            $ctrl.isNavCollapsed = !$ctrl.isNavCollapsed;
        }

        function closeNav() {
            $ctrl.isNavCollapsed = true;
        }

        function switchLang(lang) {
            $ctrl.currentLang = lang;
            store.set('lang', lang);
            $rootScope.$broadcast('language', lang);
            $state.go($state.current, {}, {reload: true});
        }

    }


})();