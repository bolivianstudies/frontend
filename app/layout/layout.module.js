(function () {
    'use strict';
    angular
        .module('app.layout', [
            'app.services',
            'app.pages',
            'app.posts',
            'app.profiles',
            'app.faq',
            'app.subscription'
        ]);
})();