(function () {
    'use strict';

    angular
        .module('app.core')
        .run(block);

    block.$inject = ['$translate', '$transitions', 'tmhDynamicLocale', 'amMoment', 'store'];

    function block($translate, $transitions, tmhDynamicLocale, amMoment, store) {

        $transitions.onStart({}, function () {
            var lang = store.get('lang') || 'es';
            $translate.use(lang);
            tmhDynamicLocale.set(lang);
            amMoment.changeLocale(lang);
        });

    }
})();