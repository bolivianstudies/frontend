(function () {
    'use strict';

    angular
        .module('app.core')
        .config(configuration);

    configuration.$inject = ['$urlRouterProvider', '$translateProvider', 'tmhDynamicLocaleProvider', 'storeProvider', 'LightboxProvider', 'cfpLoadingBarProvider'];

    function configuration($urlRouterProvider, $translateProvider, tmhDynamicLocaleProvider, storeProvider, LightboxProvider, cfpLoadingBarProvider) {

        $urlRouterProvider.otherwise('/');

        $translateProvider.useStaticFilesLoader({
            prefix: 'i18n/',
            suffix: '.json'
        });

        $translateProvider.preferredLanguage('es');
        $translateProvider.useSanitizeValueStrategy('sanitize');
        $translateProvider.useMessageFormatInterpolation();

        tmhDynamicLocaleProvider.localeLocationPattern('i18n/{{locale}}.js');

        storeProvider.setStore('localStorage');

        LightboxProvider.templateUrl = 'lightbox.template.html';
        LightboxProvider.getImageUrl = function (item) {
            return item.path || item.url;
        };

        LightboxProvider.getImageCaption = function (item) {
            return item.title;
        };

        LightboxProvider.fullScreenMode = true;

        LightboxProvider.calculateImageDimensionLimits = function (dimensions) {
            if (dimensions.windowWidth >= 768) {
                return {
                    'maxWidth': dimensions.windowWidth - 92,
                    'maxHeight': dimensions.windowHeight - 210
                };
            } else {
                return {
                    'maxWidth': dimensions.windowWidth - 52,
                    'maxHeight': dimensions.windowHeight - 86
                };
            }
        };

        LightboxProvider.calculateModalDimensions = function (dimensions) {

            var width = Math.max(400, dimensions.imageDisplayWidth + 32);

            var height = Math.max(200, dimensions.imageDisplayHeight + 150);

            if (width >= dimensions.windowWidth - 20 || dimensions.windowWidth < 768) {
                width = 'auto';
            }

            if (height >= dimensions.windowHeight) {
                height = 'auto';
            }

            return {
                'width': width,
                'height': height
            };

        };

        cfpLoadingBarProvider.includeSpinner = false;
        cfpLoadingBarProvider.includeBar = true;

    }

})();