(function () {
    'use strict';
    angular
        .module('app.core')
        .filter('moment', ['moment', function (moment) {
            return filter;

            function filter(stamp, type) {
                var filtered;
                switch (type) {
                    case 'day':
                        filtered = moment(stamp).format('DD');
                        break;
                    case 'month':
                        filtered = moment(stamp).format('MMM');
                        break;
                    case 'date':
                        filtered = moment(stamp).format('D MMM YYYY');
                        break;
                    case 'post':
                        filtered = moment(stamp).format('DD MMMM YYYY');
                        break;
                    default:
                        filtered = moment(stamp).fromNow();
                        break;
                }
                return filtered;
            }
        }]);
})();