(function () {
    'use strict';

    angular
        .module('app.core')
        .constant('angularMomentConfig', {timezone: 'America/La_Paz'})

})();