(function () {
    'use strict';

    angular
        .module('app.core', [
            'ui.bootstrap',
            'ui.router',
            'ngAnimate',
            'ngCookies',
            'ngSanitize',
            'ngTouch',
            'pascalprecht.translate',
            'tmh.dynamicLocale',
            'angularMoment',
            'angular-storage',
            'bootstrapLightbox',
            'videosharing-embed',
            'angular-loading-bar'
        ]);

})();