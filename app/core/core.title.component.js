(function () {
    'use strict';

    var title = {
        template: '{{$ctrl.title}}',
        controller: TitleController
    };

    angular
        .module('app.core')
        .component('title', title);

    TitleController.$inject = ['$rootScope', '$scope', '$translate'];

    function TitleController($rootScope, $scope, $translate) {

        var $ctrl = this;

        $rootScope.$on('$translateChangeSuccess', function () {
            $ctrl.title = $translate.instant('home.title');
        });

        $scope.$on('title', switchTitle);

        function switchTitle(e, title) {
            $ctrl.title = title;
        }
    }

})();