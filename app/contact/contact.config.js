(function () {
    'use strict';

    angular
        .module('app.contact')
        .config(configuration);

    configuration.$inject = ['$stateProvider'];

    function configuration($stateProvider) {

        var states = [
            {name: 'contact', url: '/contacto', component: 'contact'}
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });

    }

})();