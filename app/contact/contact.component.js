(function () {
    'use strict';

    var contact = {
        templateUrl: 'contact.component.html',
        controller: ContactController
    };

    angular
        .module('app.contact')
        .component('contact', contact);

    ContactController.$inject = ['$rootScope', '$scope', '$translate', 'contactService', 'store'];

    function ContactController($rootScope, $scope, $translate, contactService, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.loading = false;
        $ctrl.contact = {
            name: '',
            email: '',
            phone: '',
            institution: '',
            message: ''
        };
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLang);
        $ctrl.send = send;

        function onInit() {

        }

        function send() {

            $ctrl.loading = true;
            $rootScope.$broadcast('notify', {
                message: $translate.instant('contact.sending'),
                type: 'info',
                read: false
            });

            return contactService.send($ctrl.contact)
                .then(handleResponse)
                .finally(handleFinally);

            function handleResponse() {
                $ctrl.contact = {
                    name: '',
                    email: '',
                    phone: '',
                    institution: '',
                    message: ''
                };
            }

            function handleFinally() {
                $ctrl.loading = false;
            }

        }

        function switchLang(e, language) {
            $ctrl.currentLang = language;
        }

    }

})();