(function () {
    'use strict';

    var education = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'education.component.html';
        }],
        controller: EducationController,
        bindings: {education: '<'}
    };

    angular
        .module('app.education')
        .component('education', education);

    EducationController.$inject = ['$scope', 'store'];

    function EducationController($scope, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);

        function onInit() {

        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
        }

    }

})();