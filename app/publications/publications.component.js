(function () {
    'use strict';

    var publications = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'publications.component.html';
        }],
        controller: PublicationsController
    };

    angular
        .module('app.publications')
        .component('publications', publications);

    PublicationsController.$inject = ['$scope', '$translate', 'publicationService', 'store'];

    function PublicationsController($scope, $translate, publicationService, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.publications = [];
        $ctrl.total = null;
        $ctrl.letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $ctrl.filters = [
            {title: $translate.instant('publications.filters.author'), key: 'author'},
            {title: $translate.instant('publications.filters.year'), key: 'year'},
            {title: $translate.instant('publications.filters.title'), key: 'title'},
            {title: $translate.instant('publications.filters.editorial'), key: 'editorial'},
            {title: $translate.instant('publications.filters.countryCity'), key: 'country_city'},
            {title: $translate.instant('publications.filters.discipline'), key: 'discipline'},
            {title: $translate.instant('publications.filters.case'), key: 'case'}
        ];
        $ctrl.searchText = null;
        $scope.params = {
            page: 1,
            limit: 20,
            letter: null,
            search: '',
            key: null
        };
        $ctrl.clear = clear;
        $ctrl.search = search;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);
        $scope.$watchGroup(['params.page', 'params.letter', 'params.search', 'params.key'], getPublications);

        function onInit() {
            getPublications();
            setParams();
        }

        function getPublications() {

            return publicationService.getPublications($scope.params).then(handlePublications);

            function handlePublications(response) {
                $ctrl.publications = response.data.data;
                $ctrl.total = response.data.total;
            }
        }

        function search() {
            $scope.params.search = $ctrl.searchText;
        }

        function clear() {
            $scope.params.search = '';
            $scope.params.letter = null;
            $scope.params.key = null;
            $ctrl.searchText = null;
        }

        function setParams() {
            if ($ctrl.limit && $ctrl.limit !== null && $ctrl.limit !== undefined && parseInt($ctrl.limit) > 0) {
                $scope.params.limit = parseInt($ctrl.limit);
            }
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
            getPublications();
        }
    }

})();