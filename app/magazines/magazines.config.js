(function () {
    'use strict';

    angular
        .module('app.magazines')
        .config(configuration);

    configuration.$inject = ['$stateProvider'];

    function configuration($stateProvider) {

        var states = [
            {
                name: 'magazine',
                url: '/publicaciones/revista/{id:int}',
                component: 'magazine',
                resolve: {
                    magazine: ['$stateParams', 'magazineService', function ($stateParams, magazineService) {
                        return magazineService.getById($stateParams.id);
                    }]
                }
            }
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });

    }

})();