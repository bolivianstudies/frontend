(function () {
    'use strict';

    var magazines = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'magazines.component.html';
        }],
        controller: MagazinesController,
        bindings: {limit: '@'}
    };

    angular
        .module('app.magazines')
        .component('magazines', magazines);


    MagazinesController.$inject = ['$scope', 'magazineService', 'store'];

    function MagazinesController($scope, magazineService, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.searchText = null;
        $scope.params = {
            page: 1,
            limit: 10,
            search: ''
        };
        $ctrl.clear = clear;
        $ctrl.search = search;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);
        $scope.$watchGroup(['params.page', 'params.search'], getMagazines);

        function onInit() {
            setParams();
            getMagazines();
        }

        function getMagazines() {

            return magazineService.getMagazines($scope.params).then(handleMagazines);

            function handleMagazines(response) {
                $ctrl.magazines = response.data.data;
                $ctrl.total = response.data.total;
            }
        }

        function search() {
            $scope.params.search = $ctrl.searchText;
        }

        function clear() {
            $scope.params.search = '';
            $ctrl.searchText = null;
        }

        function setParams() {
            if ($ctrl.limit && $ctrl.limit !== null && $ctrl.limit !== undefined && parseInt($ctrl.limit) > 0) {
                $scope.params.limit = parseInt($ctrl.limit);
            }
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
            getMagazines();
        }

    }

})();