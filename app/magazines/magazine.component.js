(function () {
    'use strict';

    var magazine = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'magazine.component.html';
        }],
        controller: MagazineController,
        bindings: {magazine: '<'}
    };

    angular
        .module('app.magazines')
        .component('magazine', magazine);

    MagazineController.$inject = ['$rootScope', '$scope', 'store'];

    function MagazineController($rootScope, $scope, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);

        function onInit() {
            $ctrl.magazine = $ctrl.magazine.data;
            switchTitle();
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
        }

        function switchTitle() {
            $rootScope.$broadcast('title', $ctrl.magazine.title);
        }
    }

})();