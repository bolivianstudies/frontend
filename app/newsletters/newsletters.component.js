(function () {
    'use strict';

    var newsletters = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'newsletters.component.html';
        }],
        controller: NewslettersController,
        bindings: {limit: '@'}
    };

    angular
        .module('app.newsletters')
        .component('newsletters', newsletters);


    NewslettersController.$inject = ['$scope', 'newsletterService', 'store'];

    function NewslettersController($scope, newsletterService, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.newsletters = [];
        $ctrl.total = null;
        $ctrl.searchText = null;
        $scope.params = {
            page: 1,
            limit: 5,
            search: ''
        };
        $ctrl.clear = clear;
        $ctrl.search = search;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);
        $scope.$watchGroup(['params.page', 'params.search'], getNewsletters);

        function onInit() {
            setParams();
            getNewsletters();
        }

        function getNewsletters() {
            return newsletterService.getNewsletters($scope.params).then(handleNewsletters);

            function handleNewsletters(response) {
                $ctrl.newsletters = response.data.data;
                $ctrl.total = response.data.total;
            }
        }

        function search() {
            $scope.params.search = $ctrl.searchText;
        }

        function clear() {
            $scope.params.search = '';
            $ctrl.searchText = null;
        }

        function setParams() {
            if ($ctrl.limit && $ctrl.limit !== null && $ctrl.limit !== undefined && parseInt($ctrl.limit) > 0) {
                $scope.params.limit = parseInt($ctrl.limit);
            }
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
            getNewsletters();
        }

    }

})();