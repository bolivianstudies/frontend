(function () {

    'use strict';

    var videos = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'videos.component.html';
        }],
        controller: VideosController,
        bindings: {videos: '<', limit: '@'}
    };

    angular
        .module('app.videos')
        .component('videos', videos);

    VideosController.$inject = ['$scope', 'videoService', 'Lightbox', 'store'];

    function VideosController($scope, videoService, Lightbox, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.searchText = null;
        $ctrl.params = {
            page: 1,
            limit: 5,
            search: ''
        };
        $ctrl.clear = clear;
        $ctrl.search = search;
        $ctrl.lightBox = lightBox;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLang);

        function onInit() {
            setParams();
            getVideos();
        }

        function getVideos() {
            if (!$ctrl.videos) {
                return videoService.getVideos($ctrl.params).then(handleVideos);
            }
            function handleVideos(response) {
                return $ctrl.videos = response.data;
            }
        }

        function setParams() {
            if ($ctrl.limit && $ctrl.limit !== null && $ctrl.limit !== undefined && parseInt($ctrl.limit) > 0) {
                $ctrl.params.limit = parseInt($ctrl.limit);
            }
        }

        function clear() {
            $ctrl.params.search = '';
            $ctrl.searchText = null;
        }

        function search() {
            $ctrl.params.search = $ctrl.searchText;
        }

        function lightBox(index) {
            Lightbox.openModal($ctrl.videos, index, {});
        }

        function switchLang(e, language) {
            $ctrl.currentLang = language;
        }
    }

})();