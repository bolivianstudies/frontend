(function () {
    'use strict';

    var question = {
        templateUrl: 'faq.question.component.html',
        controller: FaqQuestionController,
        bindings: {item: '<'}
    };

    angular
        .module('app.faq')
        .component('question', question);

    FaqQuestionController.$inject = ['$scope', 'store'];

    function FaqQuestionController($scope, store) {

        var $ctrl = this;
        $ctrl.currentLang = store.get('lang') || 'es';
        $scope.$on('language', switchLanguage);
        $ctrl.$onInit = onInit;

        function onInit() {
            $ctrl.item = $ctrl.item.data;
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
        }


    }

})();