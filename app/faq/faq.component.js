(function () {
    'use strict';

    var faq = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'faq.component.html';
        }],
        controller: FaqController,
        bindings: {faq: '<', limit: '@'}
    };

    angular
        .module('app.faq')
        .component('faq', faq);

    FaqController.$inject = ['$scope', 'faqService', 'store'];

    function FaqController($scope, faqService, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.searchText = null;
        $ctrl.clear = clear;
        $ctrl.search = search;
        $ctrl.toggleAnswer = toggleAnswer;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);
        $scope.$watchGroup(['params.search', 'params.page'], getFaq);

        $scope.params = {
            page: 1,
            limit: 10,
            search: ''
        };

        function onInit() {
            setParams();
            getFaq();
        }

        function getFaq() {

            if ($ctrl.faq && $ctrl.faq.hasOwnProperty('data')) {
                $ctrl.faq = $ctrl.faq.data.data;
                $ctrl.total = $ctrl.faq.data.total;
            } else {
                return faqService.getFaqs($scope.params).then(handleFaq);
            }

            function handleFaq(response) {
                $ctrl.faq = response.data.data;
                $ctrl.total = response.data.total;
                $ctrl.faq.forEach(function (item) {
                    item.isCollapsed = true;
                });
            }
        }

        function search() {
            $scope.params.search = $ctrl.searchText;
        }

        function clear() {
            $scope.params.search = '';
            $ctrl.searchText = null;
        }

        function setParams() {
            if ($ctrl.limit !== null && $ctrl.limit !== undefined && parseInt($ctrl.limit) > 0) {
                $scope.params.limit = $ctrl.limit;
            }
        }

        function toggleAnswer(item) {
            item.isCollapsed = !item.isCollapsed;
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
            getFaq();
        }
    }

})();