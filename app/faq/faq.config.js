(function () {
    'use strict';

    angular
        .module('app.faq')
        .config(configuration);

    configuration.$inject = ['$stateProvider'];

    function configuration($stateProvider) {

        var states = [
            {
                name: 'faq',
                url: '/contacto/preguntas-frecuentes',
                component: 'faq',
                resolve: {
                    faq: ['faqService', function (faqService) {
                        return faqService.getFaqs();
                    }]
                }
            },
            {
                name: 'question',
                url: '/contacto/preguntas-frecuentes/{id:int}',
                component: 'question',
                resolve: {
                    item: ['$stateParams', 'faqService', function ($stateParams, faqService) {
                        return faqService.getById($stateParams.id);
                    }]
                }
            }
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });

    }

})();