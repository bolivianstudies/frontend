(function () {
    'use strict';

    var directory = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'directory.component.html';
        }],
        controller: DirectoryController
    };

    angular
        .module('app.directory')
        .component('directory', directory);

    DirectoryController.$inject = ['$scope', '$stateParams', 'directoryService'];

    function DirectoryController($scope, $stateParams, directoryService) {

        var $ctrl = this;

        $scope.params = {
            year: 2017
        };

        $ctrl.current = {};
        $ctrl.directory = [];
        $ctrl.directories = [];
        $ctrl.$onInit = onInit;

        $scope.$watch('params.year', changeDirectory);

        function onInit() {
            getDirectories()
                .then(handleDirectories);

            function handleDirectories(directories) {
                if ($stateParams.year) {
                    $scope.params.year = $stateParams.year;
                } else {
                    changeDirectory();
                }
            }
        }

        function changeDirectory() {
            $ctrl.current = $ctrl.directories.filter(matchYear)[0];
            function matchYear(directory) {
                return directory.id === $scope.params.year;
            }
            getDirectors();
        }

        function getDirectors() {

            return directoryService.getDirectors($scope.params).then(handleDirectors);

            function handleDirectors(response) {
                return $ctrl.directory = response.data;
            }
        }

        function getDirectories() {
            return directoryService.getDirectories().then(handleDirectories);

            function handleDirectories(response) {
                return $ctrl.directories = response.data;
            }
        }
    }

})();