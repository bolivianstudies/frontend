(function () {
    'use strict';

    var post = {
        templateUrl: 'post.component.html',
        controller: PostController,
        bindings: {post: '<'}
    };

    angular
        .module('app.posts')
        .component('post', post);

    PostController.$inject = ['$rootScope'];

    function PostController($rootScope) {

        var $ctrl = this;

        $ctrl.$onInit = onInit;

        function onInit() {
            $ctrl.post = $ctrl.post.data;
            switchTitle();
        }

        function switchTitle() {
            $rootScope.$broadcast('title', $ctrl.post.title);
        }
    }

})();