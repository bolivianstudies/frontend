(function () {
    'use strict';

    angular
        .module('app.posts')
        .filter('postTags', postTags);

    postTags.$inject = [];

    function postTags() {

        return filter;

        function filter(tags) {

            tags = tags.map(function (tag) {
                return tag.title;
            });

            return tags.join(', ');
        }
    }

})();