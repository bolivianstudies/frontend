(function () {
    'use strict';

    var posts = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'posts.component.html';
        }],
        controller: PostsController,
        bindings: {posts: '<', limit: '@', ignore: '<', tag: '<'}
    };

    angular
        .module('app.posts')
        .component('posts', posts);

    PostsController.$inject = ['$rootScope', '$scope', '$translate', 'postService', 'store'];

    function PostsController($rootScope, $scope, $translate, postService, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.posts = [];
        $ctrl.total = null;
        $ctrl.searchText = null;
        $scope.params = {
            page: 1,
            limit: 3,
            search: '',
            ignore: null,
            tag: null
        };
        $ctrl.clear = clear;
        $ctrl.search = search;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);
        $scope.$watchGroup(['params.page', 'params.search'], getPosts);
        $ctrl.title = $translate.instant('posts.title');
        $rootScope.$on('$translateChangeSuccess', function () {
            $ctrl.title = $translate.instant('posts.title');
            switchTitle();
        });

        function onInit() {
            setParams();
            getPosts();
            switchTitle();
        }

        function getPosts() {

            return postService.getPosts($scope.params).then(handlePosts);

            function handlePosts(response) {
                $ctrl.posts = response.data.data;
                $ctrl.total = response.data.total;
            }
        }

        function setParams() {
            if ($ctrl.limit !== null && $ctrl.limit !== undefined && parseInt($ctrl.limit) > 0) {
                $scope.params.limit = parseInt($ctrl.limit);
            }
            if ($ctrl.ignore !== null && $ctrl.ignore !== undefined && parseInt($ctrl.ignore) > 0) {
                $scope.params.ignore = parseInt($ctrl.ignore);
            }
            if ($ctrl.tag !== null && $ctrl.tag !== undefined && angular.isString($ctrl.tag) > 0) {
                $scope.params.tag = $ctrl.tag;
            }
        }

        function search() {
            $scope.params.search = $ctrl.searchText;
        }

        function clear() {
            $scope.params.search = '';
            $ctrl.searchText = null;
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
            getPosts();
        }

        function switchTitle() {
            if (!$ctrl.ignore && !$ctrl.limit && !$ctrl.tag) {
                $rootScope.$broadcast('title', $ctrl.title);
            }
        }

    }

})();