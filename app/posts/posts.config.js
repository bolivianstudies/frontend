(function () {
    'use strict';

    angular
        .module('app.posts')
        .config(configuration);

    configuration.$inject = ['$stateProvider'];

    function configuration($stateProvider) {

        var states = [
            {name: 'posts', url: '/noticias', component: 'posts'},
            {
                name: 'post', url: '/noticias/:slug', component: 'post',
                resolve: {
                    post: ['$stateParams', 'postService', function ($stateParams, postService) {
                        return postService.getBySlug($stateParams.slug);
                    }]
                }
            }
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });

    }

})();