(function () {
    'use strict';

    var content = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'content.component.html';
        }],
        controller: ContentController,
        require: '^slug',
        bindings: {slug: '@'}
    };

    angular
        .module('app.content')
        .component('content', content);

    ContentController.$inject = ['$scope', 'contentService', 'store'];

    function ContentController($scope, contentService, store) {
        var $ctrl = this;

        $ctrl.content = {};
        $ctrl.currentLang = store.get('lang') || 'es';
        $scope.$on('language', switchLanguage);
        $ctrl.$onInit = onInit;

        function onInit() {
            getContent();
        }

        function getContent() {
            return contentService.getBySlug($ctrl.slug)
                .then(handleContent);

            function handleContent(content) {
                return $ctrl.content = content.data;
            }
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
            getContent();
        }

    }

})();