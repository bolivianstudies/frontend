(function () {
    'use strict';
    angular.module('app', [
        'app.core',
        'app.layout',
        'app.notifications',
        'app.errors',
        'app.auth',
        'app.profiles',
        'app.education',
        'app.home',
        'app.pages',
        'app.albums',
        'app.videos',
        'app.events',
        'app.posts',
        'app.comments',
        'app.docs',
        'app.directory',
        'app.timeline',
        'app.faq',
        'app.contact',
        'app.content',
        'app.pricing',
        'app.magazines',
        'app.registration',
        'app.subscription',
        'app.paypal',
        'app.templates',
        'app.layout',
        'app.services'
    ]);
})();
