(function () {
    'use strict';

    var timeline = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'timeline.component.html';
        }],
        controller: TimelineController,
        bindings: {timeline: '<'}
    };

    angular
        .module('app.timeline')
        .component('timeline', timeline);

    TimelineController.$inject = ['timelineService'];

    function TimelineController(timelineService) {

        var $ctrl = this;

        $ctrl.params = {
            page: 10,
            limit: 1,
            search: null
        };

        $ctrl.$onInit = onInit;

        function onInit() {
            getTimeline();
        }

        function getTimeline() {
            if (!$ctrl.timeline) {
                return timelineService.getTimeline($ctrl.params).then(handleTimeline);
            }
            function handleTimeline(response) {
                return $ctrl.timeline = response.data;
            }

        }

    }

})();