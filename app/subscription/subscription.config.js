(function () {
    'use strict';

    angular
        .module('app.subscription')
        .config(configuration);

    configuration.$inject = ['$stateProvider'];

    function configuration($stateProvider) {
        var states = [
            {
                name: 'subscriptionActivation',
                url: '/subscripcion/activar/:token',
                component: 'subscriptionActivation'
            },
            {
                name: 'subscriptionSettings',
                url: '/subscripcion/configurar/:token',
                component: 'subscriptionSettings',
                resolve: {
                    subscriber: ['$stateParams', 'subscriptionService', function ($stateParams, subscriptionService) {
                        return subscriptionService.getSubscriber($stateParams.token);
                    }]
                }
            }
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });
    }

})();