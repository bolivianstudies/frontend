(function () {
    'use strict';

    var subscription = {
        templateUrl: 'subscription.component.html',
        controller: SubscriptionController
    };

    angular
        .module('app.subscription')
        .component('subscription', subscription);

    SubscriptionController.$inject = ['$scope', '$rootScope', '$translate', 'subscriptionService', 'store'];

    function SubscriptionController($scope, $rootScope, $translate, subscriptionService, store) {

        var $ctrl = this;
        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.loading = false;
        $ctrl.subscriber = {name: '', email: ''};

        $ctrl.subscribe = subscribe;
        $scope.$on('language', switchLanguage);

        function subscribe() {

            $ctrl.loading = true;

            $rootScope.$broadcast('notify', {
                message: $translate.instant('subscription.messages.subscribing'),
                type: 'info',
                read: false
            });

            return subscriptionService.subscribe($ctrl.subscriber)
                .then(handleSuccess)
                .catch(handleError);

            function handleSuccess() {
                $ctrl.loading = false;
                $ctrl.subscriber = {name: '', email: ''};
            }

            function handleError() {
                $ctrl.loading = false;
            }

        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
        }

    }

})();