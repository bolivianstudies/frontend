(function () {
    'use strict';

    var subscriptionSettings = {
        templateUrl: 'subscription.settings.component.html',
        controller: SubscriptionSettingsController,
        bindings: {subscriber: '<'}
    };

    angular
        .module('app.subscription')
        .component('subscriptionSettings', subscriptionSettings);

    SubscriptionSettingsController.$inject = ['subscriptionService'];

    function SubscriptionSettingsController(subscriptionService) {

        var $ctrl = this;

        $ctrl.loading = false;
        $ctrl.updateSubscriber = updateSubscriber;
        $ctrl.$onInit = onInit;

        function onInit() {
            $ctrl.subscriber = $ctrl.subscriber.data;
            $ctrl.subscriber.active = $ctrl.subscriber.active === 1;
            $ctrl.subscriber.posts = $ctrl.subscriber.posts === 1;
            $ctrl.subscriber.events = $ctrl.subscriber.events === 1;
            $ctrl.subscriber.magazines = $ctrl.subscriber.magazines === 1;
            $ctrl.subscriber.docs = $ctrl.subscriber.docs === 1;
            $ctrl.subscriber.newsletters = $ctrl.subscriber.newsletters === 1;
            $ctrl.subscriber.publications = $ctrl.subscriber.publications === 1;
        }

        function updateSubscriber() {

            $ctrl.loading = true;

            return subscriptionService.updateSubscription($ctrl.subscriber.activation_token, $ctrl.subscriber)
                .then(handleResponse)
                .catch(handleError);

            function handleResponse() {
                $ctrl.loading = false;
            }

            function handleError() {
                $ctrl.loading = false;
            }

        }
    }

})();