(function () {
    'use strict';

    var subscriptionActivation = {
        templateUrl: 'subscription.activation.component.html',
        controller: SubscriptionActivationController
    };

    angular
        .module('app.subscription')
        .component('subscriptionActivation', subscriptionActivation);

    SubscriptionActivationController.$inject = ['$stateParams', 'subscriptionService'];

    function SubscriptionActivationController($stateParams, subscriptionService) {

        var $ctrl = this;

        $ctrl.loading = true;
        $ctrl.success = false;
        $ctrl.failure = false;
        $ctrl.token = $stateParams.token;
        $ctrl.$onInit = onInit;

        function onInit() {
            activate();
        }

        function activate() {
            return subscriptionService.updateSubscription($ctrl.token, {active: 1})
                .then(handleResponse)
                .catch(handleError);

            function handleResponse() {
                $ctrl.loading = false;
                $ctrl.success = true;
            }

            function handleError() {
                $ctrl.loading = false;
                $ctrl.failure = true;
            }

        }
    }

})();