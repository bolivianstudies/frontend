(function () {
    'use strict';

    var home = {
        templateUrl: 'home.component.html',
        controller: HomeController,
        bindings: {home: '<'}
    };

    angular
        .module('app.home')
        .component('home', home);

    HomeController.$inject = ['$rootScope', '$scope', '$translate', 'settingsService', 'store'];

    function HomeController($rootScope, $scope, $translate, settingsService, store) {
        var $ctrl = this;
        var settings = settingsService.getSettings();

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.eventsEnabled = settings.events.enabled;

        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);

        function onInit() {
            $ctrl.home = $ctrl.home.data;
            switchTitle();
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
        }

        function switchTitle() {
            $rootScope.$broadcast('title', $translate.instant('home.title'));
        }

    }

})();