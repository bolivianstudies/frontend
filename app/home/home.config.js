(function () {
    'use strict';

    angular
        .module('app.home')
        .config(configuration);

    configuration.$inject = ['$stateProvider'];

    function configuration($stateProvider) {

        var states = [
            {
                name: 'home',
                url: '/',
                component: 'home',
                resolve: {
                    home: ['pageService', function (pageService) {
                        return pageService.getBySlug('home');
                    }]
                }
            }
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });

    }

})();