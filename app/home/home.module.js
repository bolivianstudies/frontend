(function () {
    'use strict';

    angular.module('app.home', [
        'app.services',
        'app.events',
        'app.content',
        'app.pricing'
    ]);

})();