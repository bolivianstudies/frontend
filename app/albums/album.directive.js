(function () {
    'use strict';

    angular
        .module('app.albums')
        .directive('album', album);

    album.$inject = ['$timeout', 'Lightbox', 'store'];

    function album($timeout, Lightbox, store) {

        var directive = {
            templateUrl: 'album.directive.html',
            scope: {
                photos: '<'
            },
            link: link
        };

        return directive;

        function link(scope, elem) {

            scope.activePhoto = 0;
            scope.currentLang = store.get('lang') || 'es';
            scope.params = {
                page: 1,
                limit: 5
            };
            scope.lightBox = lightBox;
            scope.$on('language', switchLang);

            function lightBox(photos, index) {
                Lightbox.openModal(photos, index, {});
            }

            function switchLang(e, language) {
                scope.currentLang = language;
            }

            scope.$watch('activePhoto', function (newVal, oldVal) {
                if (newVal !== oldVal) {
                    $timeout(function () {
                        var
                            indicators = $(elem).find('.carousel-indicators'),
                            active = indicators.find('> .active'),
                            pOffset = active.position().left
                            ;
                        pOffset += (indicators.scrollLeft());
                        indicators.animate({
                            scrollLeft: ( pOffset )
                        }, 300);
                    }, 300);
                }
            });
        }

    }

})();