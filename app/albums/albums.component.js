(function () {

    'use strict';

    var albums = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'albums.component.html';
        }],
        controller: AlbumsController,
        bindings: {albums: '<', limit: '@'}
    };

    angular
        .module('app.albums')
        .component('albums', albums);

    AlbumsController.$inject = ['$scope', 'albumService', 'Lightbox', 'store'];

    function AlbumsController($scope, albumService, Lightbox, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.searchText = null;
        $ctrl.params = {
            page: 1,
            limit: 5,
            search: ''
        };
        $ctrl.clear = clear;
        $ctrl.search = search;
        $ctrl.lightBox = lightBox;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLang);

        function onInit() {
            setParams();
            getAlbums();
        }

        function getAlbums() {
            if (!$ctrl.albums) {
                return albumService.getAlbums($ctrl.params).then(handleAlbums);
            }
            function handleAlbums(response) {
                return $ctrl.albums = response.data;
            }
        }

        function setParams() {
            if ($ctrl.limit && $ctrl.limit !== null && $ctrl.limit !== undefined && parseInt($ctrl.limit) > 0) {
                $ctrl.params.limit = parseInt($ctrl.limit);
            }
        }

        function clear() {
            $ctrl.params.search = '';
            $ctrl.searchText = null;
        }

        function search() {
            $ctrl.params.search = $ctrl.searchText;
        }

        function lightBox(photos, index) {
            Lightbox.openModal(photos, index, {});
        }

        function switchLang(e, language) {
            $ctrl.currentLang = language;
        }
    }

})();