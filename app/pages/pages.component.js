(function () {
    'use strict';

    var pages = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'pages.component.html';
        }],
        controller: PagesController
    };

    angular
        .module('app.pages')
        .component('pages', pages);

    PagesController.$inject = ['$scope', '$stateParams', '$transitions', 'pageService', 'store'];

    function PagesController($scope, $stateParams, $transitions, pageService, store) {
        var $ctrl = this;

        $ctrl.pages = [];
        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.switchLang = switchLanguage;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);

        $transitions.onSuccess({}, function () {
            $ctrl.currentTab = $stateParams.tab || null;
        });

        function onInit() {
            getPages();
        }

        function getPages() {

            return pageService.all().then(handleSuccess);

            function handleSuccess(response) {
                $ctrl.pages = response.data;
                $ctrl.pages.forEach(function (page) {
                    page.collapse = true;
                });
            }
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
            getPages();
        }

    }

})();