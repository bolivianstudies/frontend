(function () {
    'use strict';

    angular.module('app.pages', [
        'app.directory',
        'app.content',
        'app.pricing',
        'app.registration',
        'app.profiles',
        'app.timeline',
        'app.publications',
        'app.magazines',
        'app.newsletters',
        'app.posts'
    ]);

})();