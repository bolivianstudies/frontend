(function () {
    'use strict';

    var page = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'page.component.html';
        }],
        controller: PageController,
        bindings: {currentPage: '<', page: '<', siblings: '<'}
    };

    angular
        .module('app.pages')
        .component('page', page);

    PageController.$inject = ['$rootScope', '$scope', '$stateParams', 'pageService', 'store'];

    function PageController($rootScope, $scope, $stateParams, pageService, store) {
        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.currentTab = $stateParams.tab || null;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);
        $scope.$watchGroup(['currentPage', 'page'], switchTitle);

        function onInit() {
            setPages();
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
            getPage();
            getTab();
            getSiblings();
        }

        function getPage() {
            return pageService.getBySlug($stateParams.page).then(handleCurrentPage);

            function handleCurrentPage(response) {
                return $scope.currentPage = response.data;
            }
        }

        function getTab() {
            return pageService.getBySlug($ctrl.currentTab).then(handlePage);

            function handlePage(response) {
                return $scope.page = response.data;
            }
        }

        function getSiblings() {

            return pageService.getSiblings($ctrl.currentTab).then(handleSiblings);

            function handleSiblings(response) {
                return $scope.siblings = response.data;
            }
        }

        function setPages() {
            if ($ctrl.currentPage.data) {
                $scope.currentPage = $ctrl.currentPage.data;
            }
            if ($ctrl.page.data) {
                $scope.page = $ctrl.page.data;
            }
            if ($ctrl.siblings.data) {
                $scope.siblings = $ctrl.siblings.data;
            }
        }

        function switchTitle() {
            $rootScope.$broadcast('title', $scope.currentPage.title + ' > ' + $scope.page.title);
        }

    }

})();