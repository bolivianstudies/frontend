(function () {
    'use strict';

    angular
        .module('app.pages')
        .config(configuration);

    configuration.$inject = ['$stateProvider'];

    function configuration($stateProvider) {

        var states = [
            {
                name: 'page',
                url: '/:page/{tab:[a-zA-Z-]+}',
                component: 'page',
                resolve: {
                    currentPage: ['$stateParams', 'pageService', function ($stateParams, pageService) {
                        return pageService.getBySlug($stateParams.page);
                    }],
                    page: ['$stateParams', 'pageService', function ($stateParams, pageService) {
                        return pageService.getBySlug($stateParams.tab);
                    }],
                    siblings: ['$stateParams', 'pageService', function ($stateParams, pageService) {
                        return pageService.getSiblings($stateParams.tab);
                    }]
                }
            }
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });

    }

})();