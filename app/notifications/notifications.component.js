(function () {
    'use strict';

    var notifications = {
        templateUrl: 'notifications.component.html',
        controller: NotificationController
    };

    angular
        .module('app.notifications')
        .component('notifications', notifications);


    NotificationController.$inject = ['$scope', '$filter', '$timeout', 'moment'];

    function NotificationController($scope, $filter, $timeout, moment) {

        var self = this;

        self.notifications = [];
        self.notify = notify;
        self.response = response;
        self.close = close;

        $scope.$on('notify', function (e, notification) {
            notify(notification);
        });

        $scope.$on('response', function (e, data) {
            response(data);
        });

        function notify(notification) {
            notification['read'] = false;
            notification['show'] = true;
            notification['moment'] = moment().fromNow();
            $timeout(function () {
                close(notification);
            }, 3000);
            if (!$filter('filter')(self.notifications, notification).length) {
                self.notifications.push(notification);
            }
        }

        function response(response) {
            var message, type = 'info';

            if (response.hasOwnProperty('status')) {
                if (response.status >= 200 && response.status < 300) {
                    type = 'success';
                }
                if (response.status >= 300 && response.status < 400) {
                    type = 'info';
                }
                if (response.status >= 400 && response.status < 500) {
                    type = 'warning';
                }
                if (response.status >= 500) {
                    type = 'danger';
                }
            }

            if (response.hasOwnProperty('message') && angular.isString(response.message)) {
                notify({message: response.message, type: type, read: false});
            }

            else if (response.hasOwnProperty('message') && angular.isObject(response.message)) {
                for (var key in response.message) {
                    if (response.message.hasOwnProperty(key)) {
                        if (angular.isArray(response.message[key])) {
                            angular.forEach(response.message[key], function (message) {
                                notify({message: message, type: type, read: false});
                            });
                        }
                        else if (angular.isString(response.message[key])) {
                            notify({message: response.message[key], type: type, read: false});
                        }
                    }
                }
            }
        }

        function close(notification) {
            notification.show = false;
        }
    }

})();