(function () {
    'use strict';

    var comments = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'comments.component.html';
        }],
        controller: CommentsController,
        bindings: {comments: '<', type: '@', identifier: '@'}
    };

    angular
        .module('app.comments')
        .component('comments', comments);

    CommentsController.$inject = ['$rootScope', '$scope', '$translate', 'commentService', 'store'];

    function CommentsController($rootScope, $scope, $translate, commentService, store) {
        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.loading = false;
        $ctrl.addComment = {
            author: '',
            email: '',
            content: ''
        };
        $scope.params = {
            page: 1,
            limit: 10,
            search: '',
            type: null,
            identifier: null
        };
        $ctrl.send = send;
        $ctrl.clear = clear;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);
        $scope.$watchGroup(['params.page', 'params.search'], getComments);

        function onInit() {
            setParams();
            getComments();
        }

        function getComments() {

            if (!$ctrl.comments) {
                return commentService.getComments($scope.params).then(handleComments);
            }

            function handleComments(response) {
                $ctrl.comments = response;
            }
        }

        function send() {

            $ctrl.loading = true;
            $rootScope.$broadcast('notify', {
                message: $translate.instant('comments.messages.sending'),
                type: 'info',
                read: false
            });

            return commentService.send($ctrl.addComment).then(handleResponse);

            function handleResponse(response) {
                $ctrl.loading = false;
                $rootScope.$broadcast('notify', response.data);
                $ctrl.addComment = {
                    author: '',
                    email: '',
                    content: ''
                };
            }
        }

        function clear() {
            $ctrl.addComment = {
                author: '',
                email: '',
                content: ''
            };
        }

        function setParams() {
            if ($ctrl.type !== null && $ctrl.type !== undefined) {
                $scope.params.type = $ctrl.type;
            }
            if ($ctrl.identifier !== null && $ctrl.identifier !== undefined && parseInt($ctrl.identifier) > 0) {
                $scope.params.identifier = $ctrl.identifier;
            }
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
        }

    }

})();