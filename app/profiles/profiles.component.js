(function () {
    'use strict';

    var profiles = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'profiles.component.html';
        }],
        controller: profilesController,
        bindings: {limit: '@', order: '@', sort: '@'}
    };

    angular
        .module('app.profiles')
        .component('profiles', profiles);

    profilesController.$inject = ['$scope', 'profileService'];

    function profilesController($scope, profileService) {

        var $ctrl = this;

        $ctrl.profiles = [];
        $ctrl.letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $ctrl.searchText = null;
        $scope.params = {
            page: 1,
            limit: 5,
            letter: null,
            search: '',
            order: null,
            sort: null
        };
        $ctrl.clear = clear;
        $ctrl.search = search;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);
        $scope.$watchGroup(['params.page', 'params.letter', 'params.search'], getProfiles);

        function onInit() {
            setParams();
            getProfiles();
        }

        function getProfiles() {
            return profileService.getProfiles($scope.params).then(handleProfiles);

            function handleProfiles(response) {
                $ctrl.profiles = response.data.data;
                $ctrl.total = response.data.total;
            }
        }

        function search() {
            $scope.params.search = $ctrl.searchText;
        }

        function clear() {
            $scope.params.search = '';
            $scope.params.letter = null;
            $ctrl.searchText = null;
        }

        function setParams() {
            if ($ctrl.limit && $ctrl.limit !== null && $ctrl.limit !== undefined && parseInt($ctrl.limit) > 0) {
                $scope.params.limit = parseInt($ctrl.limit);
            }
            if ($ctrl.order && $ctrl.order !== null && $ctrl.order !== undefined) {
                $scope.params.order = $ctrl.order;
            }
            if ($ctrl.sort && $ctrl.sort !== null && $ctrl.sort !== undefined) {
                $scope.params.sort = $ctrl.sort;
            }
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
        }

    }

})();