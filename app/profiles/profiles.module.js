(function () {
    'use strict';
    angular.module('app.profiles', [
        'app.education',
        'app.posts',
        'app.docs',
        'app.albums',
        'app.videos'
    ]);
})();