(function () {

    'use strict';

    var profile = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'profile.component.html';
        }],
        controller: ProfileController,
        bindings: {profile: '<'}
    };

    angular
        .module('app.profiles')
        .component('profile', profile);

    ProfileController.$inject = ['$scope', 'store'];

    function ProfileController($scope, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';

        $scope.$on('language', switchLanguage);
        $ctrl.$onInit = onInit;

        function onInit() {
            $ctrl.profile = $ctrl.profile.data;
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
        }
    }

})();