(function () {
    'use strict';

    angular
        .module('app.profiles')
        .config(configuration);

    configuration.$inject = ['$stateProvider'];

    function configuration($stateProvider) {
        var states = [
            {
                name: 'profile', url: '/perfil/:id', component: 'profile', auth: true,
                resolve: {
                    profile: ['profileService', '$stateParams', function (profileService, $stateParams) {
                        return profileService.getProfile($stateParams.id);
                    }]
                }
            }
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });
    }

})();