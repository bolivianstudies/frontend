(function () {
    'use strict';

    var errors = {
        templateUrl: 'errors.component.html',
        controller: ErrorsController
    };

    angular
        .module('app.errors')
        .component('errors', errors);

    ErrorsController.$inject = ['$state'];

    function ErrorsController($state) {
        var $ctrl = this;

        $ctrl.$onInit = onInit;

        function onInit() {
            $ctrl.currentError = $state.current.name;
        }

    }

})();