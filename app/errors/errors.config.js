(function () {
    'use strict';

    angular
        .module('app.errors')
        .config(configuration);

    configuration.$inject = ['$stateProvider'];

    function configuration($stateProvider) {

        var states = [
            {name: '401', url: '/401', component: 'errors'},
            {name: '403', url: '/403', component: 'errors'},
            {name: '404', url: '/404', component: 'errors'}
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });

    }

})();