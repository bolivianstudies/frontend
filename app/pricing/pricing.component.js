(function () {
    'use strict';

    var pricing = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'pricing.component.html';
        }],
        controller: PricingController,
        bindings: {pricing: '<', page: '@', event: '@'}
    };

    angular
        .module('app.pricing')
        .component('pricing', pricing);

    PricingController.$inject = ['$scope', 'pricingService', 'store'];

    function PricingController($scope, pricingService, store) {
        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $scope.params = {
            page: null,
            event: null
        };
        $ctrl.$onInit = onInit;

        function onInit() {
            setParams();
        }

        function getPricing() {
            if (!$ctrl.pricing) {
                return pricingService.getPricing($scope.params).then(handlePricing);
            }
            function handlePricing(response) {
                return $ctrl.pricing = response.data;
            }
        }

        function setParams() {
            if ($ctrl.page !== null && $ctrl.page !== undefined && angular.isString($ctrl.page)) {
                $scope.params.page = $ctrl.page;
            }
            if ($ctrl.event !== null && $ctrl.event !== undefined && parseInt($ctrl.event) > 0) {
                $scope.params.event = $ctrl.event;
            }
            getPricing();
        }

    }

})();