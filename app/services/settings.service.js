(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('settingsService', settingsService);

    /**
     * A REST endpoint will be needed to fetch organization information
     */
    function settingsService() {

        var service = {
            getSettings: getSettings
        };

        function getSettings() {
            return {
                phone: {
                    value: '(591) (2) 222 1234',
                    enabled: false
                },
                email: {
                    value: 'estudiosbolivianos.aeb@gmail.com',
                    enabled: true
                },
                events: {
                    enabled: false
                },
                subscription: {
                    enabled: false
                },
                footer: {
                    enabled: false
                }
            };
        }

        return service;
    }

})();