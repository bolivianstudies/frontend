(function () {
    'use strict';

    angular
        .module('app')
        .factory('interceptorService', interceptorService);

    interceptorService.$inject = ['$rootScope', '$q', '$state', '$translate', 'store'];

    function interceptorService($rootScope, $q, $state, $translate, store) {

        var service = {
            request: request,
            response: response,
            responseError: responseError
        };

        return service;

        function request(config) {

            config.headers['Accept-Language'] = store.get('lang') || 'es';

            var message;

            switch (config.method) {
                case 'GET':
                    message = 'interceptor.loading';
                    break;
                case 'POST':
                    message = 'interceptor.processing';
                    break;
                case 'PUT':
                    message = 'interceptor.updating';
                    break;
                case 'DELETE':
                    message = 'interceptor.deleting';
                    break;
                default:
                    message = 'interceptor.loading';
                    break;
            }

            $rootScope.$on('$translateChangeSuccess', function () {
                $rootScope.$broadcast('notify', {
                    message: $translate.instant(message),
                    type: 'info'
                });
            });

            if (store.get('token')) {
                config.headers['Authorization'] = 'Bearer ' + store.get('token');
            }
            return config;
        }

        function response(response) {
            $rootScope.$broadcast('response', response.data);
            if (response.headers()['Authorization']) {
                store.set('token', response.headers()['Authorization']);
            }
            return response;
        }

        function responseError(rejection) {
            $rootScope.$broadcast('response', rejection.data);
            switch (rejection.status) {
                case 401:
                    store.remove('user');
                    store.remove('token');
                    $state.go('401');
                    break;
                case 404:
                    $state.go('404');
                    break;
                case 403:
                    $state.go('403');
                    break;
            }
            return $q.reject(rejection);
        }
    }

})();