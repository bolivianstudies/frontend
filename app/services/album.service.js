(function () {
    'use strict';

    angular
        .module('app.albums')
        .factory('albumService', albumService);

    albumService.$inject = ['$http', '$q', '$translate', 'API'];

    function albumService($http, $q, $translate, API) {
        var service = {
            getAlbums: getAlbums,
            getById: getById
        };

        return service;

        function getAlbums(params) {
            return $http.get(API + 'albums.json', {params: params});
        }

        function getById(id) {
            var deferred = $q.defer();

            $http.get(API + 'albums.json', {params: {id: id}})
                .then(handleAlbums)
                .catch(handleError);

            function handleAlbums(response) {
                var filtered = [], albums = response.data;

                albums.forEach(function (album) {
                    if (parseInt(album.id) === parseInt(id)) {
                        filtered.push(album);
                    }
                });

                if (filtered.length === 1) {
                    deferred.resolve(filtered[0]);
                } else {
                    handleError({
                        message: $translate.instant('albums.messages.notFound'),
                        type: 'danger',
                        status: 404,
                        read: false
                    });
                }

            }

            function handleError(response) {
                deferred.reject(response);
            }

            return deferred.promise;
        }
    }

})();