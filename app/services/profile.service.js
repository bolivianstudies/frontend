(function () {

    'use strict';

    angular
        .module('app.profiles')
        .factory('profileService', profileService);

    profileService.$inject = ['$http', 'API'];

    function profileService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            getProfiles: getProfiles,
            getProfile: getProfile
        };

        return service;

        function getProfiles(params) {
            return $http.get(API + 'profiles', {params: params});
        }

        function getProfile(id) {
            return $http.get(API + 'profile/' + id);
        }
    }

})();