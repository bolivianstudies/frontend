(function () {
    'use strict';

    angular
        .module('app.events')
        .factory('eventService', eventService);

    eventService.$inject = ['$http', 'API'];

    function eventService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            getEvents: getEvents,
            getById: getById
        };

        return service;

        function getEvents(params) {
            return $http.get(API + 'events', {params: params});
        }

        function getById(id) {
            return $http.get(API + 'event/' + id);
        }

    }

})();