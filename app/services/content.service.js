(function () {
    'use strict';

    angular.module('app.content')
        .factory('contentService', contentService);

    contentService.$inject = ['$http', 'API'];

    function contentService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            getBySlug: getBySlug
        };

        return service;

        function getBySlug(slug) {
            return $http.get(API + 'contents/' + slug);
        }
    }

})();