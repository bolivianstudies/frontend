(function () {
    'use strict';

    angular
        .module('app.newsletters')
        .factory('newsletterService', newsletterService);

    newsletterService.$inject = ['$http', 'API'];

    function newsletterService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            getNewsletters: getNewsletters
        };

        return service;

        function getNewsletters(params) {
            return $http.get(API + 'newsletters', {params: params});
        }
    }

})();