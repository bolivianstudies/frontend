(function () {
    'use strict';

    angular
        .module('app.magazines')
        .factory('magazineService', magazineService);

    magazineService.$inject = ['$http', 'API'];

    function magazineService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            getMagazines: getMagazines,
            getById: getById
        };

        return service;

        function getMagazines(params) {
            return $http.get(API + 'magazines', {params: params});
        }

        function getById(id) {
            return $http.get(API + 'magazine/' + id);
        }
    }

})();