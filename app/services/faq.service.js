(function () {
    'use strict';

    angular
        .module('app.faq')
        .factory('faqService', faqService);

    faqService.$inject = ['$http', 'API'];

    function faqService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            getFaqs: getFaqs,
            getById: getById
        };

        return service;

        function getFaqs(params) {
            return $http.get(API + 'faqs', {params: params});
        }

        function getById(id) {
            return $http.get(API + 'faq/' + id);
        }

    }

})();