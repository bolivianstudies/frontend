(function () {
    'use strict';

    angular
        .module('app.subscription')
        .factory('subscriptionService', subscriptionService);

    subscriptionService.$inject = ['$http', 'API'];

    function subscriptionService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            subscribe: subscribe,
            getSubscriber: getSubscriber,
            updateSubscription: updateSubscription
        };

        return service;

        function subscribe(subscriber) {
            return $http.post(API + 'subscriber', subscriber);
        }

        function getSubscriber(token) {
            return $http.get(API + 'subscriber/' + token);
        }

        function updateSubscription(token, subscription) {
            return $http.put(API + 'subscriber/' + token, subscription);
        }

    }

})();