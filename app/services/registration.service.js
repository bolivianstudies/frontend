(function () {
    'use strict';

    angular
        .module('app.registration')
        .factory('registrationService', registrationService);

    registrationService.$inject = ['$http', '$q', '$timeout', 'API'];

    function registrationService($http, $q, $timeout, API) {

        var service = {
            enroll: enroll
        };

        return service;

        function enroll(registration) {
            var deferred = $q.defer();

            $http.get(API + 'registration.json', {params: registration})
                .then(handleResponse)
                .catch(handleError);

            function handleResponse(response) {
                $timeout(function () {
                    deferred.resolve(response);
                }, 1500);
            }

            function handleError(response) {
                deferred.reject(response);
            }

            return deferred.promise;
        }
    }

})();