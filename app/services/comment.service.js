(function () {
    'use strict';

    angular
        .module('app.comments')
        .factory('commentService', commentService);

    commentService.$inject = ['$http', '$q', '$timeout', 'API'];

    function commentService($http, $q, $timeout, API) {

        var service = {
            getComments: getComments,
            send: send
        };

        return service;

        function getComments(params) {
            var deferred = $q.defer();

            $http.get(API + 'comments.json', {params: params})
                .then(handleComments)
                .catch(handleError);

            function handleComments(response) {
                var filtered = [], comments = response.data;

                comments.forEach(function (comment) {
                    if (comment.hasOwnProperty(params.type + '_id')) {
                        if (parseInt(comment[params.type + '_id']) === parseInt(params.identifier)) {
                            filtered.push(comment);
                        }
                    }
                });

                deferred.resolve(filtered);

            }

            function handleError(response) {
                deferred.reject(response);
            }

            return deferred.promise;
        }

        function send(comment) {
            var deferred = $q.defer();

            $http.get(API + 'comment.json', {params: comment})
                .then(handleResponse)
                .catch(handleError);

            function handleResponse(response) {
                $timeout(function () {
                    deferred.resolve(response);
                }, 1500);
            }

            function handleError(response) {
                deferred.reject(response);
            }

            return deferred.promise;
        }
    }

})();