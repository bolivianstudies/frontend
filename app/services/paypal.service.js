(function () {
    'use strict';
    angular
        .module('app.paypal')
        .factory('payPalService', payPalService);

    payPalService.$inject = ['$http', 'API'];

    function payPalService($http, API) {
        var service = {
            order: order,
            payment: payment
        };

        return service;

        function order(order) {
            return $http.get(API + 'paypal.json', {params: order});
        }

        function payment(payment) {
            return $http.post(API + 'paypal/payment', payment);
        }

    }

})();