(function () {
    'use strict';

    angular
        .module('app.auth')
        .factory('authService', authService);

    authService.$inject = ['$http', '$q', '$window', '$timeout', 'API', 'store'];

    function authService($http, $q, $window, $timeout, API, store) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            login: login,
            isAuthenticated: isAuthenticated,
            logout: logout,
            remind: remind,
            social: social
        };

        return service;

        function parseJwt(token) {
            var
                base64Url = token.split('.')[1],
                base64 = base64Url.replace('-', '+').replace('_', '/');
            return JSON.parse($window.atob(base64));
        }

        function login(credentials) {

            var deferred = $q.defer();

            $http.post(API + 'auth/login', credentials)
                .then(handleResponse)
                .catch(handleError);

            function handleResponse(response) {
                store.set('token', response.data.token);
                store.set('user', response.data.user);
                deferred.resolve();
            }

            function handleError(response) {
                deferred.reject(response);
            }

            return deferred.promise;
        }

        function social(credentials) {

            var deferred = $q.defer();

            $http.post(API + 'auth/social', credentials)
                .then(handleResponse)
                .catch(handleError);

            function handleResponse(response) {
                store.set('token', response.data.token);
                store.set('user', response.data.user);
                deferred.resolve();
            }

            function handleError(response) {
                deferred.reject(response);
            }

            return deferred.promise;
        }

        function isAuthenticated() {
            var token = store.get('token');
            if (token) {
                var params = parseJwt(token);
                return Math.round(new Date().getTime() / 1000) <= params.exp;
            } else {
                return false;
            }
        }

        function logout() {
            var deferred = $q.defer();
            $timeout(function () {
                store.remove('token');
                store.remove('user');
                deferred.resolve();
            }, 1500);
            return deferred.promise;
        }

        function remind(credentials) {
            return $http.post(API + 'auth/remind', credentials);
        }

    }

})();