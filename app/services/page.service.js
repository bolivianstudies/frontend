(function () {
    'use strict';

    angular
        .module('app.pages')
        .factory('pageService', pageService);

    pageService.$inject = ['$http', 'API'];

    function pageService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            all: all,
            getBySlug: getBySlug,
            getSiblings: getSiblings
        };

        return service;

        function all() {
            return $http.get(API + 'pages');
        }

        function getBySlug(slug) {
            return $http.get(API + 'page/' + slug);
        }

        function getSiblings(slug) {
            return $http.get(API + 'page/' + slug, {params: {siblings: true}});
        }

    }

})();