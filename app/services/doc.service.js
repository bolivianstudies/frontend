(function () {

    'use strict';

    angular
        .module('app.docs')
        .factory('docService', docService);

    docService.$inject = ['$http', '$q', 'API'];

    function docService($http, $q, API) {

        var service = {
            getDocs: getDocs
        };

        return service;

        function getDocs(params) {
            return $http.get(API + 'docs.json', {params: params});
        }

    }

})();