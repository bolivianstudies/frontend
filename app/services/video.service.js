(function () {
    'use strict';

    angular
        .module('app.videos')
        .factory('videoService', videoService);

    videoService.$inject = ['$http', '$q', '$translate', 'API'];

    function videoService($http, $q, $translate, API) {
        var service = {
            getVideos: getVideos,
            getById: getById
        };

        return service;

        function getVideos(params) {
            return $http.get(API + 'videos.json', {params: params});
        }

        function getById(id) {
            var deferred = $q.defer();

            $http.get(API + 'videos.json', {params: {id: id}})
                .then(handleVideos)
                .catch(handleError);

            function handleVideos(response) {
                var filtered = [], videos = response.data;

                videos.forEach(function (video) {
                    if (parseInt(video.id) === parseInt(id)) {
                        filtered.push(video);
                    }
                });

                if (filtered.length === 1) {
                    deferred.resolve(filtered[0]);
                } else {
                    handleError({
                        message: $translate.instant('videos.messages.notFound'),
                        type: 'danger',
                        status: 404,
                        read: false
                    });
                }

            }

            function handleError(response) {
                deferred.reject(response);
            }

            return deferred.promise;
        }
    }

})();