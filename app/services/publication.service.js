(function () {
    'use strict';

    angular
        .module('app.publications')
        .factory('publicationService', publicationService);

    publicationService.$inject = ['$http', 'API'];

    function publicationService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            getPublications: getPublications
        };

        return service;

        function getPublications(params) {
            return $http.get(API + 'publications', {params: params});
        }
    }

})();