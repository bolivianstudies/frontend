(function () {
    'use strict';

    angular.module('app.pricing')
        .factory('pricingService', pricingService);

    pricingService.$inject = ['$http', 'API'];

    function pricingService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            getPricing: getPricing
        };

        return service;

        function getPricing(params) {
            return $http.get(API + 'pricing', {params: params});
        }

    }

})();