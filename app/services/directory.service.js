(function () {
    'use strict';

    angular
        .module('app.directory')
        .factory('directoryService', directoryService);

    directoryService.$inject = ['$http', 'API'];

    function directoryService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            getDirectors: getDirectors,
            getDirectories: getDirectories
        };

        return service;

        function getDirectors(params) {
            return $http.get(API + 'directors', {params: params});
        }

        function getDirectories() {
            return $http.get(API + 'directories');
        }
    }

})();