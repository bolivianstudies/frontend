(function () {

    'use strict';

    angular
        .module('app.contact')
        .factory('contactService', contactService);

    contactService.$inject = ['$http', 'API'];

    function contactService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            send: send
        };

        return service;

        function send(contact) {
            return $http.post(API + 'messages', contact);
        }

    }

})();