(function () {
    'use strict';

    angular
        .module('app.timeline')
        .factory('timelineService', timelineService);

    timelineService.$inject = ['$http', 'API'];

    function timelineService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            getTimeline: getTimeline
        };

        return service;

        function getTimeline(params) {
            return $http.get(API + 'timeline', {params: params});
        }

    }

})();