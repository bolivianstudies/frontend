(function () {
    'use strict';

    angular
        .module('app.posts')
        .factory('postService', postService);

    postService.$inject = ['$http', 'API'];

    function postService($http, API) {

        API = 'http://bolivianstudies:8000/';

        var service = {
            getPosts: getPosts,
            getBySlug: getBySlug
        };

        return service;

        function getPosts(params) {
            return $http.get(API + 'posts', {params: params});
        }

        function getBySlug(slug) {
            return $http.get(API + 'post/' + slug);
        }
    }

})();