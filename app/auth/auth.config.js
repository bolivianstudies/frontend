(function () {
    'use strict';

    angular
        .module('app.auth')
        .config(configuration);

    configuration.$inject = ['$stateProvider', '$httpProvider', 'authProvider', 'jwtInterceptorProvider'];

    function configuration($stateProvider, $httpProvider, authProvider, jwtInterceptorProvider) {

        var states = [
            {name: 'login', url: '/ingresar', component: 'login'},
            {name: 'reset', url: '/restablecer', component: 'reset'},
            {name: 'logout', url: '/salir', component: 'logout'}
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });

        authProvider.init({
            domain: 'bolivianstudies2.auth0.com',
            clientID: 'SfUK9tqGbbEPWA5aHk9jgPQLp5XIBrSo',
            loginUrl: '/ingresar'
        });

        jwtInterceptorProvider.tokenGetter = tokenGetter;

        tokenGetter.$inject = ['store'];

        function tokenGetter(store) {
            return store.get('auth0token');
        }

        $httpProvider.interceptors.push('jwtInterceptor');

    }

})();