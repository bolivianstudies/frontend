(function () {
    'use strict';

    var reset = {
        templateUrl: 'auth.reset.component.html',
        controller: ResetController
    };

    angular
        .module('app.auth')
        .component('reset', reset);

    ResetController.$inject = ['$rootScope', '$translate', 'authService'];

    function ResetController($rootScope, $translate, authService) {

        var $ctrl = this;

        $ctrl.credentials = {email: ''};
        $ctrl.loading = false;
        $ctrl.remind = remind;

        function remind() {
            $ctrl.loading = true;

            $rootScope.$broadcast('notify', {
                message: $translate.instant('reset.messages.verifying'),
                type: 'info',
                read: false
            });

            return authService.remind($ctrl.credentials)
                .then(handleSuccess)
                .catch(handleError);

            function handleSuccess() {
                $ctrl.credentials.email = '';
                $ctrl.loading = false;
            }

            function handleError() {
                $ctrl.loading = false;
            }
        }

    }

})();