(function () {
    'use strict';

    angular
        .module('app.auth')
        .run(block);

    block.$inject = ['$transitions', '$state', 'authService', 'auth', 'jwtHelper', 'store'];

    function block($transitions, $state, authService, auth, jwtHelper, store) {

        auth.hookEvents();

        $transitions.onStart({}, function ($transition$) {

            var toState = $transition$.$to();

            if (toState.name === 'login' && authService.isAuthenticated()) {
                $state.go('profile', {id: store.get('user').id});
            }

            if (!auth.isAuthenticated) {
                var
                    token = store.get('auth0token'),
                    profile = store.get('profile');
                if (token) {
                    if (!jwtHelper.isTokenExpired(token)) {
                        auth.authenticate(profile, token);
                    }
                }
            }

        });

    }
})();