(function () {
    'use strict';

    var logout = {
        templateUrl: 'auth.logout.component.html',
        controller: LogoutController
    };

    angular
        .module('app.auth')
        .component('logout', logout);

    LogoutController.$inject = ['$rootScope', '$state', '$translate', 'authService'];

    function LogoutController($rootScope, $state, $translate, authService) {

        authService.logout().then(handleLogout);

        function handleLogout() {
            $rootScope.$broadcast('notify', {
                message: $translate.instant('logout.success'),
                type: 'success',
                read: false
            });
            $state.go('home');
        }
    }

})();