(function () {
    'use strict';

    var login = {
        templateUrl: 'auth.login.component.html',
        controller: LoginController
    };

    angular
        .module('app.auth')
        .component('login', login);

    LoginController.$inject = ['$rootScope', '$state', '$translate', 'authService', 'auth', 'store'];

    function LoginController($rootScope, $state, $translate, authService, auth, store) {

        var $ctrl = this;

        $ctrl.credentials = {
            email: '',
            password: ''
        };

        $ctrl.loading = false;
        $ctrl.login = login;
        $ctrl.social = social;

        function login() {

            $ctrl.loading = true;

            $rootScope.$broadcast('notify', {
                message: $translate.instant('login.messages.authenticating'),
                type: 'info',
                read: false
            });

            return authService.login($ctrl.credentials)
                .then(handleLogin)
                .catch(handleError);

            function handleLogin() {
                var user = store.get('user');
                $rootScope.$broadcast('notify', {
                    message: $translate.instant('login.welcome') + user.name,
                    type: 'success',
                    read: false
                });
                $state.go($state.current, {}, {reload: true});
            }

            function handleError() {
                $ctrl.loading = false;
            }
        }

        function social(connection) {
            auth.signin({
                popup: true,
                connection: connection
            }, function (profile, token) {
                authService.social({
                    id: profile.user_id,
                    name: profile.name,
                    email: profile.email
                }).then(function () {
                    $state.go($state.current, {}, {reload: true});
                });
                store.set('profile', profile);
                store.set('auth0token', token);
            });
        }

    }

})();