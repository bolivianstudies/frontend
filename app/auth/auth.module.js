(function () {
    'use strict';

    angular.module('app.auth', [
        'auth0',
        'angular-jwt'
    ]);

})();