(function () {
    'use strict';

    var event = {
        templateUrl: 'event.component.html',
        controller: EventController,
        bindings: {event: '<'}
    };

    angular
        .module('app.events')
        .component('event', event);

    EventController.$inject = ['$rootScope', '$scope', '$stateParams', 'uiGmapGoogleMapApi', 'store'];

    function EventController($rootScope, $scope, $stateParams, uiGmapGoogleMapApi, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.showMap = false;
        $scope.$on('language', switchLanguage);
        $ctrl.$onInit = onInit;

        function onInit() {
            $ctrl.event = $ctrl.event.data;
            switchTitle();
            var markers = [];
            angular.forEach($ctrl.event.location.markers, function (marker) {
                markers.push({id: marker.id, idKey: marker.id, latitude: marker.latitude, longitude: marker.longitude});
            });
            uiGmapGoogleMapApi.then(function () {
                if ($ctrl.event.location.latitude && $ctrl.event.location.longitude) {
                    $ctrl.map = {
                        id: $ctrl.event.id,
                        center: {
                            latitude: $ctrl.event.location.latitude,
                            longitude: $ctrl.event.location.longitude
                        },
                        zoom: 15,
                        markers: markers
                    };
                    $ctrl.showMap = true;
                }
            });
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
            getEvent();
        }

        function getEvent() {
            if ($stateParams.id) {
                return eventService.getById($stateParams.id).then(handleEvent);
            }

            function handleEvent(response) {
                return $ctrl.event = response;
            }
        }

        function switchTitle() {
            $rootScope.$broadcast('title', $ctrl.event.title);
        }


    }

})();