(function () {
    'use strict';

    var events = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'events.component.html';
        }],
        controller: EventsController,
        bindings: {limit: '@'}
    };

    angular
        .module('app.events')
        .component('events', events);

    EventsController.$inject = ['$rootScope', '$scope', '$translate', 'eventService', 'store'];

    function EventsController($rootScope, $scope, $translate, eventService, store) {
        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.title = $translate.instant('events.title');
        $ctrl.events = [];
        $ctrl.total = null;
        $ctrl.searchText = null;
        $scope.params = {
            page: 1,
            limit: 3,
            search: ''
        };

        $ctrl.search = search;
        $ctrl.clear = clear;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);
        $scope.$watchCollection('params', getEvents);
        $ctrl.title = $translate.instant('events.title');
        $rootScope.$on('$translateChangeSuccess', function () {
            $ctrl.title = $translate.instant('events.title');
            switchTitle();
        });

        function onInit() {
            setParams();
            switchTitle();
        }

        function getEvents() {

            return eventService.getEvents($scope.params).then(handleEvents);

            function handleEvents(response) {
                $ctrl.events = response.data.data;
                $ctrl.total = response.data.total;
            }
        }

        function setParams() {
            if ($ctrl.limit !== null && $ctrl.limit !== undefined && parseInt($ctrl.limit) > 0) {
                $scope.params.limit = $ctrl.limit;
            } else {
                getEvents();
            }
        }

        function search() {
            $scope.params.search = $ctrl.searchText;
        }

        function clear() {
            $scope.params.search = '';
            $ctrl.searchText = null;
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
            getEvents();
        }

        function switchTitle() {
            if (!$ctrl.limit) {
                $rootScope.$broadcast('title', $ctrl.title);
            }
        }

    }

})();