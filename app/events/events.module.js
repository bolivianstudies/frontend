(function () {
    'use strict';

    angular.module('app.events', ['uiGmapgoogle-maps', 'app.albums']);

})();