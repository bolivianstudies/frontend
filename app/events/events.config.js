(function () {
    'use strict';

    angular
        .module('app.events')
        .config(configuration);

    configuration.$inject = ['$stateProvider', 'uiGmapGoogleMapApiProvider'];

    function configuration($stateProvider, uiGmapGoogleMapApiProvider) {

        var states = [
            {name: 'events', url: '/eventos', component: 'events'},
            {
                name: 'event',
                url: '/eventos/{id}',
                component: 'event',
                resolve: {
                    event: ['$stateParams', 'eventService', function ($stateParams, eventService) {
                        return eventService.getById($stateParams.id);
                    }]
                }
            }
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });
        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyDKuZCUVKjta7c6viDmJQSicJWmr_JSV2o',
            v: '3.22',
            libraries: 'places'
        });

    }

})();