(function () {
    'use strict';

    var docs = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'docs.component.html';
        }],
        controller: DocsController,
        bindings: {docs: '<', identifier: '@', type: '@'}
    };

    angular
        .module('app.docs')
        .component('docs', docs);

    DocsController.$inject = ['$scope', 'docService', 'store'];

    function DocsController($scope, docService, store) {

        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.searchText = null;
        $ctrl.params = {
            page: 1,
            limit: 5,
            search: ''
        };
        $ctrl.clear = clear;
        $ctrl.search = search;
        $ctrl.$onInit = onInit;
        $scope.$on('language', switchLanguage);

        function onInit() {
            setParams();
            getDocs();
        }

        function getDocs() {
            if (!$ctrl.docs) {
                return docService.getDocs($ctrl.params).then(handleDocs);
            }
            function handleDocs(response) {
                return $ctrl.docs = response.data;
            }
        }

        function setParams() {
            if ($ctrl.type !== null && $ctrl.type !== undefined) {
                $ctrl.params.type = $ctrl.type;
            }
            if ($ctrl.identifier !== null && $ctrl.identifier !== undefined && parseInt($ctrl.identifier) > 0) {
                $ctrl.params.identifier = $ctrl.identifier;
            }
        }

        function search() {
            $ctrl.params.search = $ctrl.searchText;
        }

        function clear() {
            $ctrl.params.search = '';
            $ctrl.searchText = null;
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
        }

    }

})();