(function () {
    'use strict';
    angular.module('app.registration', ['vcRecaptcha', 'app.paypal']);
})();