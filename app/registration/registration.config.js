(function () {
    'use strict';

    angular
        .module('app.registration')
        .config(configuration);

    configuration.$inject = ['$stateProvider'];

    function configuration($stateProvider) {

        var states = [
            {name: 'register', url: '/registro', component: 'registration'}
        ];

        states.forEach(function (state) {
            $stateProvider.state(state);
        });

    }

})();