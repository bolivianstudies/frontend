(function () {
    'use strict';

    var registration = {
        templateUrl: ['$element', '$attrs', function ($element, $attrs) {
            return $attrs.templateUrl || 'registration.component.html';
        }],
        controller: RegistrationController

    };

    angular
        .module('app.registration')
        .component('registration', registration);

    RegistrationController.$inject = ['$rootScope', '$scope', '$translate', 'registrationService', 'store'];

    function RegistrationController($rootScope, $scope, $translate, registrationService, store) {
        var $ctrl = this;

        $ctrl.currentLang = store.get('lang') || 'es';
        $ctrl.loading = false;
        $ctrl.inscription = {
            name: '',
            email: '',
            address: '',
            city: '',
            country: '',
            zip: '',
            phone: '',
            discipline: '',
            institution: '',
            profession: '',
            membership: ''
        };
        $ctrl.billing = {
            name: '',
            email: '',
            address: '',
            city: '',
            country: '',
            zip: '',
            phone: ''
        };
        $ctrl.captcha = false;
        $scope.sameBilling = false;

        $ctrl.$onInit = onInit;
        $ctrl.enroll = enroll;
        $scope.$watch('sameBilling', checkBilling);
        $scope.$on('language', switchLanguage);

        function onInit() {
        }

        function enroll() {

            $ctrl.loading = true;

            $rootScope.$broadcast('notify', {
                message: $translate.instant('registration.messages.processing'),
                type: 'info',
                read: false
            });

            return registrationService.enroll({
                inscription: $ctrl.inscription,
                billing: $ctrl.billing
            })
                .then(handleResponse)
                .catch(handleError);

            function handleResponse(response) {
                $ctrl.loading = false;
                $rootScope.$broadcast('notify', response.data);
                $ctrl.inscription = {
                    name: '',
                    email: '',
                    address: '',
                    city: '',
                    country: '',
                    zip: '',
                    phone: '',
                    discipline: '',
                    institution: '',
                    profession: '',
                    membership: ''
                };
                $ctrl.billing = {
                    name: '',
                    email: '',
                    address: '',
                    city: '',
                    country: '',
                    zip: '',
                    phone: ''
                };
                $ctrl.captcha = false;
                $scope.sameBilling = false;
            }

            function handleError(response) {
                $rootScope.$broadcast('notify', {
                    message: response.data,
                    type: 'danger',
                    read: false
                });
                $ctrl.loading = false;
            }

        }

        function checkBilling(newVal, oldVal) {
            if (newVal !== oldVal && newVal === true) {
                $ctrl.billing = $ctrl.inscription;
            } else {
                $ctrl.inscription = {
                    name: '',
                    email: '',
                    address: '',
                    city: '',
                    country: '',
                    zip: '',
                    phone: '',
                    discipline: '',
                    institution: '',
                    profession: ''
                };
            }
        }

        function switchLanguage(e, language) {
            $ctrl.currentLang = language;
        }

    }

})();